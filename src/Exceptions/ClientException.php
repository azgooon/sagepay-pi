<?php

namespace Lumnn\SagePayPi\Exceptions;

class ClientException extends BadResponseException
{
    public function getStatusCode(): ?string
    {
        if ($this->decodedBody && isset($this->decodedBody->statusCode)) {
            return $this->decodedBody->statusCode;
        }

        return null;
    }
}
