<?php

/*
 * This file is part of the SagePayPi package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lumnn\SagePayPi\Transaction\Response;

/**
 * Response for payment, which contains a bit more information than normal response.
 */
class TransactionResponse extends Response
{
    public function getBankResponseCode(): ?string
    {
        return $this->getResponseData()->bankResponseCode ?? null;
    }

    public function getCardId(): ?string
    {
        return $this->getResponseData()->card->cardIdentifier ?? ($this->getResponseData()->paymentMethod->card->cardIdentifier ?? null);
    }

    public function getAvsCvcCheckStatus(): ?string
    {
        return $this->getResponseData()->avsCvcCheck->status ?? null;
    }

    public function getAvsCvcCheckAddress(): ?string
    {
        return $this->getResponseData()->avsCvcCheck->address ?? null;
    }

    public function getAvsCvcCheckPostalCode(): ?string
    {
        return $this->getResponseData()->avsCvcCheck->postalCode ?? null;
    }

    public function getAvsCvcCheckSecurityCode(): ?string
    {
        return $this->getResponseData()->avsCvcCheck->securityCode ?? null;
    }

    public function get3dSecureStatus(): ?string
    {
        return $this->getResponseData()->{'3DSecure'}->status ?? null;
    }

    public function getThreeDSecureStatus(): ?string
    {
        return $this->get3dSecureStatus();
    }
}
