<?php

/*
 * This file is part of the SagePayPi package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lumnn\SagePayPi\Transaction;


/**
 * Authorise Transaction Request.
 */
class AuthoriseRequest extends AbstractTransactionRequest implements AuthoriseRequestInterface
{

    public string $transactionId;
    public string $applyAvsCvcCheck;

    public function __construct(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    public function getTransactionType(): string
    {
        return self::TYPE_AUTHORISE;
    }

    public function getReferenceTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setApplyAvsCvcCheck($applyAvsCvcCheck): self
    {
        $this->applyAvsCvcCheck = $applyAvsCvcCheck;

        return $this;
    }

    public function getApplyAvsCvcCheck(): string
    {
        return $this->applyAvsCvcCheck;
    }
}
