<?php

/*
 * This file is part of the SagePayPi package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Lumnn\SagePayPi\Transaction;

use Lumnn\SagePayPi\AddressInterface;

/**
 * Refund Transaction Request.
 */
class RepeatRequest extends AbstractTransactionRequest implements RepeatRequestInterface
{

    public string $transactionId;
    public string $currency;
    public ?AddressInterface $shippingDetails = null;
    public bool $giftAid = false;

    public function __construct(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    public function getTransactionType(): string
    {
        return self::TYPE_REPEAT;
    }

    public function getReferenceTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setCurrency($currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setShippingDetails(AddressInterface $shippingDetails): self
    {
        $this->shippingDetails = $shippingDetails;

        return $this;
    }

    public function getShippingDetails(): ?AddressInterface
    {
        return $this->shippingDetails;
    }

    public function setGiftAid(bool $giftAid): self
    {
        $this->giftAid = $giftAid;

        return $this;
    }

    public function getGiftAid(): bool
    {
        return $this->giftAid;
    }


}
